package com.domain.tests.api

import com.domain.tests.domain.Hotel
import com.domain.tests.util.Constant
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface ServiceInterface {


    @GET("63a9ecea18ca79c275a2eeafd95bc37f857cf2ec/1.2/0777.json")
    fun getHotelList(): Observable<List<Hotel>>

    @GET("63a9ecea18ca79c275a2eeafd95bc37f857cf2ec/1.2/{id}.json")
    fun getHotel(@Path("id") id: Int): Observable<Hotel>

    companion object Factory {
        fun create(): ServiceInterface {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Constant.BASE_URL)
                    .build()

            return retrofit.create(ServiceInterface::class.java)
        }
    }
}
