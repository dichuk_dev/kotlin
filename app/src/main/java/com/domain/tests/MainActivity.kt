package com.domain.tests

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.domain.tests.di.component.ActivityComponent
import com.domain.tests.di.component.DaggerActivityComponent
import com.domain.tests.di.module.ActivityModule
import com.domain.tests.fragment.DashboardFragment
import com.domain.tests.fragment.HotelDetailFragment
import com.domain.tests.mvp.Main
import javax.inject.Inject

class MainActivity : AppCompatActivity(), Main.View {

    @Inject
    lateinit var presenter: Main.Presenter

    private val component: ActivityComponent by lazy {
        DaggerActivityComponent
                .builder()
                .activityModule(ActivityModule(this))
                .build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        component.inject(this)

        presenter.initView(this)
    }

    fun getMainPresenter(): Main.Presenter {
        return presenter
    }

    override fun showDashboardFragment() {
        supportFragmentManager.beginTransaction()
                .disallowAddToBackStack()
                .replace(R.id.fr_container, DashboardFragment().newInstance(), "DashboardFragment")
                .commit()
    }

    override fun showHotelDetailFragment(hotelID: Int) {
        supportFragmentManager.beginTransaction()
                .addToBackStack("DashboardFragment")
                .replace(R.id.fr_container, HotelDetailFragment().newInstance(hotelID), "HotelDetailFragment")
                .commit()
    }

}
