package com.domain.tests.mvp

import com.domain.tests.domain.Hotel
import com.domain.tests.mvp.core.Base

class HotelView {

    interface View : Base.View {
        fun onProgress(isProgress: Boolean)
        fun onSuccessData(hotel: Hotel)
        fun onSuccessData(list: List<Hotel>)
    }

    interface Presenter : Base.Presenter<View> {
        fun loadData(id: Int)
        fun loadDataAll()
    }

}