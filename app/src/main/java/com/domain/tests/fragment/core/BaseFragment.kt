package com.domain.tests.fragment.core

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.domain.tests.MainActivity

abstract class BaseFragment : Fragment() {

    private lateinit var baseView: View

    open fun newInstance(): BaseFragment {
        return this
    }

    open fun restoreBundle() {

    }

    protected abstract fun getLayoutResource(): Int

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        restoreBundle()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerPresenter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        baseView = inflater.inflate(getLayoutResource(), container, false)
        initUI(baseView)
        return baseView
    }

    protected fun getMainActivity(): MainActivity {
        return activity as MainActivity
    }

    protected abstract fun registerPresenter()

    protected abstract fun initUI(baseView: View)

}