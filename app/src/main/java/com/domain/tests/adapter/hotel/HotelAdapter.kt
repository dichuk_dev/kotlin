package com.domain.tests.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.domain.tests.R
import com.domain.tests.adapter.core.BaseRecyclerViewAdapter
import com.domain.tests.adapter.core.BaseViewWrapper
import com.domain.tests.adapter.hotel.HotelViewHolder
import com.domain.tests.domain.Hotel

class HotelAdapter : BaseRecyclerViewAdapter<Hotel, HotelViewHolder>() {

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): HotelViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.view_holder_hotel, parent, false)
        return HotelViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(viewHolder: BaseViewWrapper<HotelViewHolder>, position: Int) {
        val holder = viewHolder.getView()
        val item = getItems()[position]
        holder.bind(item, position)
    }
}


