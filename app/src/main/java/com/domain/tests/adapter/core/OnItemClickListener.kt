package com.domain.tests.adapter.core

interface OnItemClickListener<T> {
    fun onItemClick(t: T, position: Int)
}