package com.domain.tests.adapter.core

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout

abstract class BaseViewHolder<T>(view: View, protected var onItemClickListener: OnItemClickListener<T>) : LinearLayout(view.context), View.OnClickListener {

    init {
        this.addView(view)
        this.layoutParams = RecyclerView.LayoutParams(
                RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT)

        this.initUI()
        this.setListener()
    }

    protected abstract fun initUI()

    protected open fun setListener() {

    }

    override fun onClick(view: View) {

    }

    abstract fun bind(t: T, position: Int)

}