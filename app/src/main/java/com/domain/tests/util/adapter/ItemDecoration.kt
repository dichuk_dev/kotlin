package com.domain.tests.util.adapter

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.support.annotation.DimenRes
import android.view.View


class ItemDecoration(@param:DimenRes private val spaceRes: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val value = view.resources.getDimensionPixelSize(spaceRes)
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = value
        }
        outRect.bottom = value
        outRect.left = value
        outRect.right = value
    }
}