package com.domain.tests.util

import android.graphics.Bitmap
import com.squareup.picasso.Transformation


class CropTransformation(val crop: Int) : Transformation {

    override fun key(): String {
        return "CropTransformation()"
    }

    override fun transform(source: Bitmap?): Bitmap {
        val width = source!!.width
        val height = source.height
        val scale = crop * 2
        val croppedBmp = Bitmap.createBitmap(source, scale, scale, width - scale * 2, height - scale * 2)
        source.recycle()
        return croppedBmp
    }

}