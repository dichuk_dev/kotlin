package com.domain.tests.util

class Constant {
    companion object {
        const val BASE_URL = "https://bitbucket.org/domain/tests/raw/"
        const val BASE_IMAGE_URL = "${BASE_URL}63a9ecea18ca79c275a2eeafd95bc37f857cf2ec/1.2/"
    }
}