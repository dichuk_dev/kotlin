package com.domain.tests.di.module

import com.domain.tests.api.ServiceInterface
import com.domain.tests.mvp.HotelView
import com.domain.tests.mvp.ui.HotelPresenter
import dagger.Module
import dagger.Provides

@Module
class FragmentModule {

    @Provides
    fun provideServiceApi(): ServiceInterface = ServiceInterface.create()

    @Provides
    fun provideDashboard(): HotelView.Presenter = HotelPresenter()

}