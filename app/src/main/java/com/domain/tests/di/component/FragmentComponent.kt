package com.domain.tests.di.component

import com.domain.tests.di.module.FragmentModule
import com.domain.tests.fragment.DashboardFragment
import com.domain.tests.fragment.HotelDetailFragment
import dagger.Component


@Component(modules = [FragmentModule::class])
interface FragmentComponent {
    fun inject(dashboardFragment: DashboardFragment)

    fun inject(hotelDetailFragment: HotelDetailFragment)
}