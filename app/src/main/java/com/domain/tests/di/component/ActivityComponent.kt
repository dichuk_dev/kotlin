package com.domain.tests.di.component

import com.domain.tests.MainActivity
import com.domain.tests.di.module.ActivityModule
import dagger.Component

@Component(modules = [ActivityModule::class])
interface ActivityComponent {
    fun inject(app: MainActivity)
}