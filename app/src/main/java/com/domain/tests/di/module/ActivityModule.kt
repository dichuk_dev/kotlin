package com.domain.tests.di.module

import android.support.v7.app.AppCompatActivity
import com.domain.tests.MainActivity
import com.domain.tests.mvp.Main
import com.domain.tests.mvp.ui.ActivityPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ActivityModule(val activity: MainActivity) {

    @Provides
    @Singleton
    fun provideActivity(): AppCompatActivity = activity

    @Provides
    fun providePresenter(): Main.Presenter = ActivityPresenter()

}
